var express = require('express')
var bot = require('./bot.js')
var bodyParser = require('body-parser')
var app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

var numOfBots = 2;
for (var n=1; n<=numOfBots; n++) {
  var b = bot();
  b.init();
  app.post('/'+n+'/dostep/', function (req, res) {
    var ct = req['headers']['content-type'];
    var validct = 'application/json';
    if (ct == validct) {
      res.json(b.step(req.body))
    } else {
      res.status(400).send('Only application/json content-type allowed. You sent: '+ct);
    }
  })

  app.get('/'+n+'/dopenalty/', function (req, res) {
    res.json(b.penalty());
  });

  app.get('/'+n+'/returnstep/', function (req, res) {
    b.returnstep();
    res.status(200).send();
  });

  app.get('/'+n+'/restart/', function (req, res) {
    b.restart();
    res.status(200).send();
  });

  app.get('/'+n+'/hasnext/', function (req, res) {
    res.json(b.hasNext());
  });
}

var server = app.listen(3000, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port)

})
