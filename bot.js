var fs = require('fs');

var LEFT = 0;
var RIGHT = 1;

var width = 40;
var height = 24;
var gatesize = 4;
var penanaltysize = 6;
var matrix = [];
var pos = [0,0];
var lastStep = [];
var matrixCross = [];
var gateDirection = RIGHT;
var ig; // gate col

function init() {
  if (gateDirection === RIGHT) {
    ig = width;
  } else {
    ig = 0;
  }
  pos = [0,0];
  for(var i=0; i<width+1; i++) {
      matrix[i] = [];
      for(var j=0; j<height+1; j++) {
          if (i==ig && j>=(height-gatesize)/2 && j<=(height+gatesize)/2) {
            matrix[i][j] = false;
          } else if (i==0 || i==width || j==0 || j==height) {
            matrix[i][j] = true;
          } else {
            matrix[i][j] = false;
          }
      }
  }
  for(var i=0; i<width; i++) {
      matrixCross[i] = [];
      for(var j=0; j<height; j++) {
        matrixCross[i][j] = false;
      }
  }
  var cur = postocur(pos);
  matrix[cur[0]][cur[1]] = true;
}

function postocur(pos) {
  return [pos[0]+width/2, pos[1]+height/2];
}

function getCur() {
  return [pos[0]+width/2, pos[1]+height/2];
}

function ijtopos(i,j) {
  return [i-width/2, j-height/2];
}

function getAvailableSteps() {
  var avail = [];
  var cur = getCur();

  for (var i=cur[0]-1; i<=cur[0]+1; i++) {
    for (var j=cur[1]-1; j<=cur[1]+1; j++) {
      if (typeof matrix[i] !== 'undefined' && typeof matrix[i][j] !== 'undefined' && !matrix[i][j] && !isCross(cur,i,j)) {
        avail.push(ijtopos(i,j));
      }
    }
  }
  return avail;
}

function isCross(cur,i,j) {
  try {
    if (cur[0] !== i && cur[1] !== j) {
      if (cur[0] > i && cur[1] > j && matrixCross[i][j]) {
        return true;
      } else if (cur[0] > i && cur[1] < j && matrixCross[i][j+1]) {
        return true;
      } else if (cur[0] < i && cur[1] > j && matrixCross[i+1][j]) {
        return true;
      } else if (cur[0] < i && cur[1] < j && matrixCross[i+1][j+1]) {
        return true;
      }
    }
  } catch(e) {
    console.log("isCross", e);
  }
  return false;
}

function getAvailPenalty() {
  var avail = [];
  var cur = postocur(pos);
  var minx = Math.max(0, cur[0]-penanaltysize);
  var maxx = Math.min(width+1, cur[0]+penanaltysize);
  var miny = Math.max(0, cur[1]-penanaltysize);
  var maxy = Math.min(height+1, cur[1]+penanaltysize);
  for (var i=minx; i<=maxx; i++) {
    for (var j=miny; j<=maxy; j++) {
      if (isCrossedPoint(cur, [i,j]) && typeof matrix[i] !== 'undefined' && typeof matrix[i][j] !== 'undefined' && !matrix[i][j]) {
        avail.push(ijtopos(i,j));
      }
    }
  }
  return avail;
}

function isCrossedPoint(a, b) {
  return a[0]===b[0] || a[1]===b[1] || Math.abs(a[0]-b[0])===Math.abs(a[1]-b[1]);
}

function addToCrossMatrix(steps) {
  if (steps.length > 1) {
    for (var i=0; i<steps.length-1; i++) {
      var a = steps[i];
      var b = steps[i+1];
      if (Math.abs(a[0]-b[0]) === 1 && Math.abs(a[1]-b[1]) === 1) {
        // var p1 = a[0];
        // var p2 = a[1];
        // var dx = a[0]-b[0];
        // var dy = a[1]-b[1];
        // if (dx === 1) {
        //   p1--;
        // }
        // if (dy === 1) {
        //   p2--;
        // }
        // matrixCross[p1,p2] = true;
        if(a[0]>b[0] && a[1]>b[1]){
          matrixCross[b[0],b[1]] = true;
        }
        if(a[0]>b[0] && a[1]<b[1]){
          matrixCross[b[0],a[1]] = true;
        }
        if(a[0]<b[0] && a[1]<b[1]){
          matrixCross[a[0],a[1]] = true;
        }
        if(a[0]<b[0] && a[1]>b[1]){
          matrixCross[a[0],b[1]] = true;
        }
      }

    }
  }
}

function penalty() {
  var avail = getAvailPenalty();
  if (avail.length !== 0) {
    var best = getBest(avail);
    var mp = getMidlePoits(pos, best);
    mp.forEach(function(p) {
      addtomatrix(p);
    });
    addToCrossMatrix(mp);
    pos = best;
    return mp;
  } else {
    return [pos];
  }
}

function getMidlePoits(a,b) {
  var m = [a];
  var dx = a[0]-b[0];
  var dy = a[1]-b[1];
  var xi = 0, yi = 0;
  while (dx != 0 || dy != 0) {
    if (dx < 0) {
      xi++;
    } else if (dx > 0) {
      xi--;
    }
    if (dy < 0) {
      yi++;
    } else if (dy > 0) {
      yi--;
    }
    var t = [a[0]+xi, a[1]+yi];
    var dx = t[0]-b[0];
    var dy = t[1]-b[1];
    m.push(t);
  }
  return m;
}

function addtomatrix(pos) {
  var cur = postocur(pos);
  matrix[cur[0]][cur[1]] = true;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getBest(avail) {
  var t;
  avail.forEach(function(a) {
    if (isGateDirection(a)) {
      t = a;
    }
  });
  return t ? t : avail[getRandomInt(0,avail.length-1)];
}

function isGateDirection(a) {
  return a[1] == pos[1] && ((a[0] > pos[0] && ig > pos[0]) || (a[0] < pos[0] && ig < pos[0]));
}

function getSteps() {
  var steps = [];
  steps.push(pos);
  for (var i=0; i<3; i++) {
    var avail = getAvailableSteps();
    if (avail.length !== 0) {
      var best = getBest(avail);
      pos = best;
      addtomatrix(best);
      steps.push(best);
    }
  }
  return steps;
}

function step(prev) {
  prev.forEach(function(a) {
    addtomatrix(a);
  });
  addToCrossMatrix(prev);
  if (prev.length !== 0) {
    pos = prev[prev.length-1];
  }
  var steps = getSteps();
  addToCrossMatrix(steps);
  lastStep = steps;
  return steps;
}

function returnstep() {
  fs.writeFile("/tmp/test", matrix, matrixCross);
  lastStep.forEach(function(s, i) {
    if (i !== 0) {
      var cur = postocur(s);
      matrix[cur[0]][cur[1]] = false;
    }
  });
  pos = lastStep[0];
}

function restart() {
  init();
}

function hasNext() {
  var next = 0;
  if (getAvailableSteps().length !== 0) {
    next = 1;
  }
  return {
    next: next
  }
}

module.exports = function() {
  return {
    init: init,
    penalty: penalty,
    step: step,
    returnstep: returnstep,
    restart: restart,
    hasNext: hasNext
  };
}
